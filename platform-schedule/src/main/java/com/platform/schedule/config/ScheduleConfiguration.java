package com.platform.schedule.config;

import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

@Configuration
public class ScheduleConfiguration {
		
	@Bean
	public SchedulerFactoryBean schedulerFactoryBean(DataSource dataSource) {
		SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
		schedulerFactoryBean.setDataSource(dataSource);
		schedulerFactoryBean.setSchedulerName("PlatformScheduler");

        //<!--延时启动 -->
		schedulerFactoryBean.setStartupDelay(30);
		schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContextKey");
		//<!--可选，QuartzScheduler 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了 -->
		schedulerFactoryBean.setOverwriteExistingJobs(true);
        //<!-- 设置自动启动   默认为true -->
        schedulerFactoryBean.setAutoStartup(true);
        
        Properties quartzProperties = new Properties();
        
        quartzProperties.put("org.quartz.scheduler.instanceName","PlatformScheduler");
        quartzProperties.put("org.quartz.scheduler.instanceId","AUTO");
        //  <!-- 线程池配置 -->
        quartzProperties.put("org.quartz.threadPool.class","org.quartz.simpl.SimpleThreadPool");
        quartzProperties.put("org.quartz.threadPool.threadCount","20");
        quartzProperties.put("org.quartz.threadPool.threadPriority","5");
        
        // <!-- JobStore 配置 -->
        quartzProperties.put("org.quartz.jobStore.class","org.quartz.impl.jdbcjobstore.JobStoreTX");
        
        //<!-- 集群配置 -->
        quartzProperties.put("org.quartz.jobStore.isClustered","true");
        quartzProperties.put("org.quartz.jobStore.clusterCheckinInterval","15000");
        quartzProperties.put("org.quartz.jobStore.maxMisfiresToHandleAtATime","1");
        quartzProperties.put("org.quartz.jobStore.misfireThreshold","12000");
        
        //<!-- 表前缀 -->
        quartzProperties.put("org.quartz.jobStore.tablePrefix","QRTZ_");
        
        schedulerFactoryBean.setQuartzProperties(quartzProperties);
        
        return schedulerFactoryBean;
	}
	
}
