package com.platform.admin.dao;

import com.platform.admin.entity.SysSmsLogEntity;
import com.platform.common.dao.BaseDao;

/**
 * Dao
 *
 * @author lipengjun
 * @date 2017-12-16 23:38:05
 */
public interface SysSmsLogDao extends BaseDao<SysSmsLogEntity> {

}
