package com.platform.admin.dao;

import com.platform.admin.entity.SysOssEntity;
import com.platform.common.dao.BaseDao;

/**
 * 文件上传
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-03-25 12:13:26
 */
public interface SysOssDao extends BaseDao<SysOssEntity> {

}
