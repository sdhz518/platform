package com.platform.admin.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ConditionalOnProperty(prefix = "base", name = "swagger-open", havingValue = "true")
public class SwaggerConfiguration {
	
	
    @Bean
    public Docket createRestApi() {
    	
    	//ParameterBuilder tokenPar = new ParameterBuilder();  
        //List<Parameter> pars = new ArrayList<Parameter>();  
        //tokenPar.name("appkey").description("令牌").modelRef(new ModelRef("string")).parameterType("header").required(false).build();
        
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))                         //这里采用包含注解的方式来确定要显示的接口
                .apis(RequestHandlerSelectors.basePackage("com.lorin.base.controller"))    //这里采用包扫描的方式来确定要显示的接口
                .paths(PathSelectors.any())
                .build();
        //.globalOperationParameters(operationParameters);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Lorin Base Doc")
                .description("Base Api文档")
                .termsOfServiceUrl("#")
                .version("2.0")
                .build();
    }
}
