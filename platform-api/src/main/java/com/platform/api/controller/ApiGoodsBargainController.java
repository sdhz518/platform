package com.platform.api.controller;

import com.platform.api.annotation.IgnoreAuth;
import com.platform.api.annotation.LoginUser;
import com.platform.api.entity.GoodsBargainVo;
import com.platform.api.entity.UserVo;
import com.platform.api.service.ApiGoodsBargainService;
import com.platform.api.util.ApiBaseAction;
import com.platform.api.util.ApiPageUtils;
import com.platform.common.utils.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者: @author Harmon <br>
 * 时间: 2017-08-11 08:32<br>
 * 描述: ApiIndexController <br>
 */
@RestController
@RequestMapping("/api/bargain")
public class ApiGoodsBargainController extends ApiBaseAction {
    @Autowired
    private ApiGoodsBargainService apiGoodsBargainService;

    /**
     */
    @IgnoreAuth
    @RequestMapping("list")
    public Object list(@LoginUser UserVo loginUser, @RequestParam(value = "page", defaultValue = "1") Integer page,
                       @RequestParam(value = "size", defaultValue = "10") Integer size) {
        Map param = new HashMap();
        param.put("page", page);
        param.put("limit", size);
        param.put("sidx", "id");
        param.put("order", "desc");
        //查询列表数据
        Query query = new Query(param);
        List<GoodsBargainVo> topicEntities = apiGoodsBargainService.queryList(query);
        int total = apiGoodsBargainService.queryTotal(query);
        ApiPageUtils pageUtil = new ApiPageUtils(topicEntities, total, query.getLimit(), query.getPage());
        return toResponsSuccess(pageUtil);
    }
}