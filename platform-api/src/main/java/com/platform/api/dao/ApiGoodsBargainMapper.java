package com.platform.api.dao;

import com.platform.api.entity.GoodsBargainVo;
import com.platform.common.dao.BaseDao;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-28 08:35:00
 */
public interface ApiGoodsBargainMapper extends BaseDao<GoodsBargainVo> {

}
