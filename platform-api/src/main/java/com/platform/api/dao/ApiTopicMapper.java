package com.platform.api.dao;

import com.platform.api.entity.TopicVo;
import com.platform.common.dao.BaseDao;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiTopicMapper extends BaseDao<TopicVo> {
	
}
