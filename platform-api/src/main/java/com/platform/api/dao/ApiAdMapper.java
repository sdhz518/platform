package com.platform.api.dao;

import com.platform.api.entity.AdVo;
import com.platform.common.dao.BaseDao;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:14:25
 */
public interface ApiAdMapper extends BaseDao<AdVo> {
	
}
