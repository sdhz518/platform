package com.platform.api.dao;

import com.platform.api.entity.FeedbackVo;
import com.platform.common.dao.BaseDao;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:14:26
 */
public interface ApiFeedbackMapper extends BaseDao<FeedbackVo> {
	
}
