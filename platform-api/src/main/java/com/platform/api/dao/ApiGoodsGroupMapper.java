package com.platform.api.dao;

import com.platform.api.entity.GoodsGroupVo;
import com.platform.common.dao.BaseDao;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-26 21:37:50
 */
public interface ApiGoodsGroupMapper extends BaseDao<GoodsGroupVo> {

}
