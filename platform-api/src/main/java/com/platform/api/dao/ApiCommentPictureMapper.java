package com.platform.api.dao;

import com.platform.api.entity.CommentPictureVo;
import com.platform.common.dao.BaseDao;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:14:26
 */
public interface ApiCommentPictureMapper extends BaseDao<CommentPictureVo> {
	
}
