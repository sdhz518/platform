package com.platform.api.dao;

import com.platform.api.entity.GoodsCrashVo;
import com.platform.common.dao.BaseDao;

/**
 * 商品搭配减Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-30 19:38:48
 */
public interface ApiGoodsCrashMapper extends BaseDao<GoodsCrashVo> {

}
