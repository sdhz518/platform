package com.platform.api.dao;

import com.platform.api.entity.SpecificationVo;
import com.platform.common.dao.BaseDao;

/**
 * 规格表
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiSpecificationMapper extends BaseDao<SpecificationVo> {
	
}
