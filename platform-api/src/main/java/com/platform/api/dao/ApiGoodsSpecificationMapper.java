package com.platform.api.dao;

import com.platform.api.entity.GoodsSpecificationVo;
import com.platform.common.dao.BaseDao;

/**
 * 商品对应规格表值表
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:16:46
 */
public interface ApiGoodsSpecificationMapper extends BaseDao<GoodsSpecificationVo> {
	
}
