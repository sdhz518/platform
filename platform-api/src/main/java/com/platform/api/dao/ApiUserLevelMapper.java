package com.platform.api.dao;

import com.platform.api.entity.UserLevelVo;
import com.platform.common.dao.BaseDao;

/**
 * 商城_用户级别
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-11 09:16:47
 */
public interface ApiUserLevelMapper extends BaseDao<UserLevelVo> {
	
}
