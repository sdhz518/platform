package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.AttributeCategoryEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-17 16:13:27
 */
public interface AttributeCategoryDao extends BaseDao<AttributeCategoryEntity> {

}
