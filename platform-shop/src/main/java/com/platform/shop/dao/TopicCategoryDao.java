package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.TopicCategoryEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-20 15:41:56
 */
public interface TopicCategoryDao extends BaseDao<TopicCategoryEntity> {

}
