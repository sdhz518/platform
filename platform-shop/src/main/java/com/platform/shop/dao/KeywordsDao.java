package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.KeywordsEntity;

/**
 * 热闹关键词表Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-25 21:23:41
 */
public interface KeywordsDao extends BaseDao<KeywordsEntity> {

}
