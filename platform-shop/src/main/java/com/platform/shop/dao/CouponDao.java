package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.CouponEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 12:53:26
 */
public interface CouponDao extends BaseDao<CouponEntity> {

}
