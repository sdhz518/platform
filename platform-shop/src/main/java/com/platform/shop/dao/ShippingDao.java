package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.ShippingEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-09-04 21:42:24
 */
public interface ShippingDao extends BaseDao<ShippingEntity> {

}
