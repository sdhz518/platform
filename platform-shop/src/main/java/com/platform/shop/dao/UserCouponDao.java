package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.UserCouponEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 15:40:33
 */
public interface UserCouponDao extends BaseDao<UserCouponEntity> {

}
