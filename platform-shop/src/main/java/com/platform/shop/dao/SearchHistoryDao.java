package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.SearchHistoryEntity;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:10
 */
public interface SearchHistoryDao extends BaseDao<SearchHistoryEntity> {
	
}
