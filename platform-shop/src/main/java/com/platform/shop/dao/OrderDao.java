package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.OrderEntity;

/**
 * 
 * 
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-13 10:41:09
 */
public interface OrderDao extends BaseDao<OrderEntity> {
	
}
