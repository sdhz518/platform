package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.UserLevelEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-16 16:52:22
 */
public interface UserLevelDao extends BaseDao<UserLevelEntity> {

}
