package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.GoodsCrashEntity;

/**
 * 商品搭配减Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-30 18:35:41
 */
public interface GoodsCrashDao extends BaseDao<GoodsCrashEntity> {

}
