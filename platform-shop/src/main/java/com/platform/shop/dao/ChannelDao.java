package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.ChannelEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-22 19:19:56
 */
public interface ChannelDao extends BaseDao<ChannelEntity> {

}
