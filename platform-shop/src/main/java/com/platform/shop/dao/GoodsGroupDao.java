package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.GoodsGroupEntity;

/**
 * 团购Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-09-06 17:18:30
 */
public interface GoodsGroupDao extends BaseDao<GoodsGroupEntity> {

}
