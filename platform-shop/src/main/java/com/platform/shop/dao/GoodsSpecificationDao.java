package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.GoodsSpecificationEntity;

/**
 * 商品对应规格表值表Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-31 11:15:55
 */
public interface GoodsSpecificationDao extends BaseDao<GoodsSpecificationEntity> {

}
