package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.ProductEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-30 14:31:21
 */
public interface ProductDao extends BaseDao<ProductEntity> {

}
