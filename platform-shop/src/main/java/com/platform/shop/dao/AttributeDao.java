package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.AttributeEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-17 16:48:17
 */
public interface AttributeDao extends BaseDao<AttributeEntity> {

}
