package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.AddressEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-16 17:22:46
 */
public interface AddressDao extends BaseDao<AddressEntity> {

}
