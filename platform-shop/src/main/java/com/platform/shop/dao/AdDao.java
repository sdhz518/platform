package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.AdEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 09:37:35
 */
public interface AdDao extends BaseDao<AdEntity> {

}
