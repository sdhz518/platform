package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.GoodsIssueEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-23 14:12:34
 */
public interface GoodsIssueDao extends BaseDao<GoodsIssueEntity> {

}
