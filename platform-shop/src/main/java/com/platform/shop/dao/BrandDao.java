package com.platform.shop.dao;

import com.platform.common.dao.BaseDao;
import com.platform.shop.entity.BrandEntity;

/**
 * Dao
 *
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-08-19 17:59:15
 */
public interface BrandDao extends BaseDao<BrandEntity> {

}
