package com.platform.common.cache;

import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.platform.common.dao.SysMacroDao;
import com.platform.common.entity.SysMacroEntity;
import com.platform.common.utils.SpringContextUtils;

import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

/**
 * 作者: @author Harmon <br>
 * 时间: 2017-08-16 10:14<br>
 * 描述: CacheUtil <br>
 */
@Component
@DependsOn("springContextUtils")
public class CacheUtil  {


    public static List<SysMacroEntity> sysMacroEntityList;

    @PostConstruct
    public void init() {
        SysMacroDao macroDao = SpringContextUtils.getBean(SysMacroDao.class);
        if (null != macroDao) {
            sysMacroEntityList = macroDao.queryList(new HashMap<String, Object>());
        }
    }
  

    /**
     * 根据字典标识获取字典中文
     *
     * @param value
     * @return
     */
    public static String getCodeName(String preName, String value) {
        String name = "";
        Long parentId = 0L;
        for (SysMacroEntity macroEntity : sysMacroEntityList) {
            if (value.equals(macroEntity.getValue())) {
                parentId = macroEntity.getParentId();
            }
        }
        for (SysMacroEntity macroEntity : sysMacroEntityList) {
            if (preName.equals(macroEntity.getValue()) && parentId.equals(macroEntity.getParentId())) {
                name = macroEntity.getName();
            }
        }
        return name;
    }

}