package com.platform.common.dao;


import com.platform.common.entity.SysRegionEntity;

/**
 * @author lipengjun
 * @email 939961241@qq.com
 * @date 2017-11-04 11:19:31
 */
public interface SysRegionDao extends BaseDao<SysRegionEntity> {

}
